# Ozobot Evo control library

This (Python 3 only) package provides an API for controlling an Ozobot Evo robot through its Bluetooth connection.

It has been heavily based on the retro-engineering work done 
by Raphael Bink (https://github.com/raphaelbink/raspberry-pi-ozobot-evo).

An extended version of the robot model is available in the `ozoevo.robot.Robot` class. In addition
of methods already there in the original version, it has been enhanced by providing:

- convenience methods for controlling the motors and the LEDs
- the ability to work offline, so that unit tests of client applications can be executed without
*root* privileges

A symbolic representation of the LEDs is provided by the `ozoevo.robot.LedSelector` enum. Thanks to it, 
the set of LEDs targeted by the control methods can be passed as an iterable of enum items. The set
is automatically converted into the proper bit pattern used by low level control methods.

## Discovery process

The Bluetooth discovery process is wrapped in the `ozoevo.bt.RobotFinder`, which takes care
of scanning for Bluetooth devices in the vicinity, retaining OzoBot Evos only.

The finder implements the `dict` interface, using the public name of the robot as key. Entries
of the directory are `ozoevo.bt.RobotBTDescriptor` instances, holding the MAC address of the 
device and its Bluetooth name (currently, the public name as set in the Evo application, prefixed
by `Ozo`).

Beware that the MAC address is random and changes each time the robot is turned on. The only 
persistent property is the name... as long as it is not modified by the user. 

A typical usage scenario is :

```python
from ozoevo.bt import RobotFinder
from ozoevo.robot import OzoEvo

finder = RobotFinder()
finder.find_robots()

my_bot = OzoEvo(finder['MyBotName'].mac_addr)
my_bot.drive(...)
```

The `RobotFinder.find_robots` method can be called at any time, for refreshing the directory.
Descriptors belonging to no more visible devices are discarded during the process.

## Distribution

The wheel is generated with the command `make wheel` (or `make` alone, `wheel` being the default target).
It will be produced in the `dist` sub-directory of the project, which is automatically created if
required. This target sub-directory contains only the last generated wheel. Already present ones
are moved to the `dist-arch` sub-directory before building.

To relieve from having to build the wheels from sources, the pre-built one for the latest 
released version is available for download in the `dist` sub-directory of the repository.

Previous versions are archived in the `dist-arch` sub-directory. 

## Tests

Tests use the `pytest` framework. They can be executed by several means:
- `PYTHONPATH=. pytest tests`
- `python setup.py pytest`
- `make test` *(invokes the `pytest` command under the hood)*

Either way produces the same result. Which one to choose depends on your taste ;)

## Installation

` pip install ozoevo_lib-<version>-py3-none-any.whl`

## Dependencies

Installation dependencies:
- `bluepy` (https://github.com/IanHarvey/bluepy)

They are installed automatically during the package deployment.

Development dependencies:
- `pytest` (https://pytest.org/)
- `pytest-runner` (optional, only if `setup.py` is used to run the tests)

Note: although it doesn't harm, there is no need to install `pytest-runner` beforehand, since
`setup.py` will take care ot it if needed when tests are executed by `python setup.py pytest`.

## Validation

The package has been validated with Linux Python 3.5.4. No test of any kind has been done 
for non-Linux environments *(and there are no chance I'll do it one day, since not having
such boxes)*. 