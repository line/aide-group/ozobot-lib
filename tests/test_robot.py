import unittest.mock
import time
import struct

import pytest

from ozoevo.robot import OzoEvo, NotConnectedError, LedSelector
from ozoevo.bt import MY_ROBOT_MAC


@pytest.fixture
def bot():
    return OzoEvo(MY_ROBOT_MAC)


class MockRobot(OzoEvo):
    def __init__(self, *args, **kwargs):
        kwargs['offline'] = True
        super().__init__(*args, **kwargs)


@pytest.fixture
def mocked_bot():
    with unittest.mock.patch('ozoevo.robot.OzoEvo', new=MockRobot(MY_ROBOT_MAC)) as bot:
        yield bot


def test_create():
    bot = OzoEvo(MY_ROBOT_MAC)
    assert bot
    assert not bot.is_connected


def test_needs_connection(bot):
    assert not bot.is_connected

    with pytest.raises(NotConnectedError):
        bot.all_leds_off()


def test_connected(mocked_bot):
    assert mocked_bot.is_connected is False

    mocked_bot.connect()
    assert mocked_bot.is_connected is True

    # check that the reset sequence has been issued
    assert mocked_bot._control_peripheral.data is not None

    # this time it must pass
    mocked_bot.all_leds_off()


def test_drive(mocked_bot):
    # no_reset used to be able to check peripheral usage
    mocked_bot.connect(no_reset=True)

    start = time.time()
    left_speed, right_speed = 100, 50
    mocked_bot.drive(left_speed, right_speed, 5, wait=False)
    assert time.time() - start < 1

    assert mocked_bot._drive_peripheral.data.startswith(bytes([OzoEvo.CMD_DRIVE]))
    assert mocked_bot._control_peripheral.data is None

    _, left, right, _ = struct.unpack('<Bhhh',  mocked_bot._drive_peripheral.data)
    assert left == left_speed
    assert right == right_speed

    start = time.time()
    mocked_bot.drive(left_speed, right_speed, 0.1, wait=True)
    assert time.time() - start >= 0.1


def test_spin(mocked_bot):
    mocked_bot.connect(no_reset=True)

    speed = 100
    mocked_bot.spin(speed, 1, wait=False)
    assert mocked_bot._drive_peripheral.data.startswith(bytes([OzoEvo.CMD_DRIVE]))
    assert mocked_bot._control_peripheral.data is None

    _, left, right, _ = struct.unpack('<Bhhh',  mocked_bot._drive_peripheral.data)
    assert left == speed
    assert left == -right


def test_set_leds(mocked_bot):
    # we dont' need no reset here, since the LED command will use the control peripheral anyway
    mocked_bot.connect()

    mocked_bot.set_leds([LedSelector.CENTER, LedSelector.TOP], 100, 0, 0)

    expected = bytes([OzoEvo.CMD_LED, LedSelector.CENTER.value | LedSelector.TOP.value])
    assert mocked_bot._control_peripheral.data[:len(expected)] == expected

    assert mocked_bot._drive_peripheral.data is None


def test_set_leds_single(mocked_bot):
    # we dont' need no reset here, since the LED command will use the control peripheral anyway
    mocked_bot.connect()

    mocked_bot.set_leds(LedSelector.CENTER, 100, 0, 0)

    for sent_byte, expected_value in zip(
            mocked_bot._control_peripheral.data, [OzoEvo.CMD_LED, LedSelector.CENTER.value]
    ):
        assert sent_byte == expected_value
    assert mocked_bot._drive_peripheral.data is None


def test_leds_off(mocked_bot):
    # we dont' need no reset here, since the LED command will use the control peripheral anyway
    mocked_bot.connect()

    mocked_bot.leds_off([LedSelector.CENTER])

    for sent_byte, expected_value in zip(
            mocked_bot._control_peripheral.data, [OzoEvo.CMD_LED, LedSelector.CENTER.value, 0, 0, 0]
    ):
        assert sent_byte == expected_value
    assert mocked_bot._drive_peripheral.data is None


def test_single_led_off(mocked_bot):
    # we dont' need no reset here, since the LED command will use the control peripheral anyway
    mocked_bot.connect()

    mocked_bot.leds_off(LedSelector.CENTER)

    for sent_byte, expected_value in zip(
            mocked_bot._control_peripheral.data, [OzoEvo.CMD_LED, LedSelector.CENTER.value, 0, 0, 0]
    ):
        assert sent_byte == expected_value
    assert mocked_bot._drive_peripheral.data is None


def test_all_leds_off(mocked_bot):
    # we dont' need no reset here, since the LED command will use the control peripheral anyway
    mocked_bot.connect()

    mocked_bot.all_leds_off()

    for sent_byte, expected_value in zip(
            mocked_bot._control_peripheral.data, [OzoEvo.CMD_LED, LedSelector.ALL.value, 0, 0, 0]
    ):
        assert sent_byte == expected_value
    assert mocked_bot._drive_peripheral.data is None
