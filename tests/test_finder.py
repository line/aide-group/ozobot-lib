import pytest

from ozoevo.bt import RobotFinder, MY_ROBOT_NAME, MY_ROBOT_MAC


@pytest.fixture()
def finder():
    return RobotFinder(offline=True)


def test_simulated_scan(finder):
    finder.find_robots()

    assert len(finder) > 0
    assert MY_ROBOT_NAME in finder
    assert finder[MY_ROBOT_NAME].mac_addr == MY_ROBOT_MAC


def test_robot_creation(finder):
    # should fail since no scan done yet
    with pytest.raises(KeyError):
        finder.create_robot(MY_ROBOT_NAME)

    finder.find_robots()

    # should work now
    r = finder.create_robot(MY_ROBOT_NAME)
    assert r


def test_robot_creation_with_autoscan(finder):
    # should fail since no scan done yet
    with pytest.raises(KeyError):
        finder.create_robot(MY_ROBOT_NAME)

    # should work now
    r = finder.create_robot(MY_ROBOT_NAME, rescan_before=True)
    assert r
