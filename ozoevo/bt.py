""" Bluetooth tools fo OzoBot
"""

import os
from collections import namedtuple

from bluepy import btle

from .robot import OzoEvo

__all__ = ['RobotFinder', 'RobotBTDescriptor', 'OZO_DEVICE_NAME_PREFIX', 'MY_ROBOT_MAC', 'MY_ROBOT_NAME']


OZO_DEVICE_NAME_PREFIX = 'Ozo'

RobotBTDescriptor = namedtuple('RobotBTDescriptor', 'mac_addr device_name')

MY_ROBOT_NAME = 'MyEvo'
MY_ROBOT_MAC = 'FF:FF:FF:FF:FF:FF'

FAKE_SCAN_RESULT = {
    name: RobotBTDescriptor(mac, 'Ozo' + name)
    for name, mac in (
        (MY_ROBOT_NAME, MY_ROBOT_MAC),
        ('AideLine', '01:23:45:67:89:0a'),
        ('BlackBot', 'bc:de:f0:12:34:56'),
        ('WhiteBot', '78:9a:bc:de:f0:12'),
    )
}


class RobotFinder(dict):
    """ OzoBot Evo scanner

    Wraps the Bluepy scanner to expose a dict like interface to the list of descriptors
    for OzoBots Evo within reach.

    They key is the public name of the robot, as set using the Evo app, and the value
    is an instance of :class:`RobotBTDescriptor`.
    """
    def __init__(self, offline=False, logger=None):
        super().__init__()

        self.logger = logger
        self.offline = offline
        self.scanner = btle.Scanner().withDelegate(btle.DefaultDelegate())

        if self.offline and self.logger:
            self.logger.warning(
                'The finder is configured in offline mode. Scan results will contain fake devices.'
            )

    def find_robots(self, timeout=10):
        """ Scan reachable BT devices, and updates the directory of OzoBots
        """
        self.scanner.clear()

        if not self.offline:
            if os.getuid() != 0:
                raise OSError('scan requires root privileges')

            devices = self.scanner.scan(timeout)

            self.clear()
            for dev in devices:
                for adtype, desc, dev_name in dev.getScanData():
                    if adtype == 9 and dev_name.startswith(OZO_DEVICE_NAME_PREFIX):
                        robot_name = dev_name[len(OZO_DEVICE_NAME_PREFIX):]
                        self[robot_name] = RobotBTDescriptor(dev.addr, dev_name)

        else:
            if self.logger:
                self.logger.warning(
                    'simulating fake device(s): %s',
                    ', '.join(
                        "%s(%s)" % (v.device_name, v.mac_addr)
                        for v in FAKE_SCAN_RESULT.values()
                    )
                )
            self.update(FAKE_SCAN_RESULT)

    def create_robot(self, name: str, rescan_before: bool = False, **init_parms) -> OzoEvo:
        """ Factory method for creating a robot by name.

        The name must be in the list of robot already discovered,
        otherwise a `KeyError` exception is raised.

        :param str name: the name of the robot
        :param bool rescan_before: if True a Bluetooth scan will be done
               to refresh the known robots list before creating the requested one
        :param init_parms: named parameters passed to the `Robot` class `__init__` method
        :return: an instance of the robot
        :rtype: OzoEvo
        :raises KeyError: if the name is not found in already discovered robots
        """
        if rescan_before:
            self.find_robots()

        return OzoEvo(self[name].mac_addr, **init_parms)
