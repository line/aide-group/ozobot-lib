import time
import struct
from enum import Enum
from typing import Iterable, Union, List, Set
from functools import reduce

from bluepy import btle

__all__ = [
    'OzoEvo', 'LedSelector', 'NotConnectedError'
]

# Low level bit patterns used to select the LEDs

LED_TOP = 0x01
LED_LEFT = 0x02
LED_CENTER_LEFT = 0x04
LED_CENTER = 0x08
LED_CENTER_RIGHT = 0x10
LED_RIGHT = 0x20
LED_REAR = 0x80

LED_ALL = 0xff


class LedSelector(Enum):
    """ Friendly LED selector
    """
    top = LED_TOP
    left = LED_LEFT
    center_left = LED_CENTER_LEFT
    center = LED_CENTER
    center_right = LED_CENTER_RIGHT
    right = LED_RIGHT
    rear = LED_REAR

    @classmethod
    def all(cls) -> List['LedSelector']:
        return [l for l in LedSelector]

    @classmethod
    def all_pattern(cls) -> int:
        return 0xff

    @classmethod
    def pattern(cls, leds: Set['LedSelector']) -> int:
        return reduce(lambda x, y: x | y, (l.value for l in leds))


class NotConnectedError(Exception):
    pass


def needs_connection(func):
    """ Decorator for tagging methods which need the robot to be connected.
    """
    def wrapper(self, *args, **kwargs):
        if not self.is_connected:
            raise NotConnectedError()

        return func(self, *args, **kwargs)

    return wrapper


class SimulatedPeripheral:
    """ Fake peripheral used for test to check the commands which would have been
    sent to the robot.
    """
    def __init__(self, which):
        self.which = which

        self.data = None

    def write(self, data):
        self.data = data


class OzoEvo:
    """ Models an OzoEvo robot.
    """
    TRANSMIT_UUID_DRIVE = "8903136c-5f13-4548-a885-c58779136702"
    TRANSMIT_UUID_CONTROL = "8903136c-5f13-4548-a885-c58779136703"

    CMD_DRIVE = 64
    CMD_LED = 68

    RESET_SEQUENCE = b"\x50\x02\x01"

    def __init__(self, addr, offline=False, logger=None):
        """
        :param str addr: the MAC address, as a string in xx:xx:... format
        :param offline: if True, BT traffic will be simulated (for testing)
        :param logger: optional logger
        """
        self._mac_addr = addr
        self._drive_peripheral = None
        self._control_peripheral = None
        self._is_connected = False
        self._offline = offline
        self._logger = logger

    def connect(self, force=False, no_reset=False):
        """ Connects to the robot.

        Must be done before issuing any command. The method is idempotent, and calling it
        when the robot is already connected results in a no-operation.
        """
        if self._is_connected and not force:
            return

        if not self._offline:
            peripheral = btle.Peripheral(self._mac_addr, btle.ADDR_TYPE_RANDOM)
            self._drive_peripheral = peripheral.getCharacteristics(uuid=self.TRANSMIT_UUID_DRIVE)[0]
            self._control_peripheral = peripheral.getCharacteristics(uuid=self.TRANSMIT_UUID_CONTROL)[0]
        else:
            self._drive_peripheral = SimulatedPeripheral('drive')
            self._control_peripheral = SimulatedPeripheral('control')

        if not no_reset:
            self.ready()
        self._is_connected = True

    def __str__(self):
        return "mac_addr=%s, offline=%s" % (self._mac_addr, self._offline)

    @property
    def is_connected(self):
        return self._is_connected

    @property
    def mac_addr(self):
        return self._mac_addr

    def _write_to_control(self, command):
        if self._logger:
            self._logger.info("sent to control peripheral : %s", ' '.join('%02x' % b for b in command))
        self._control_peripheral.write(command)

    def _write_to_drive(self, command):
        if self._logger:
            self._logger.info("sent to drive peripheral : %s", ' '.join('%02x' % b for b in command))
        self._drive_peripheral.write(command)

    def ready(self):
        """ Sends a stop file command to OzoBot for it to be able to lister to commands.
        """
        self._write_to_control(self.RESET_SEQUENCE)

    def disconnect(self):
        """ Logical disconnection.

        .. warning::

            This does not act at Bluetooth level.
        """
        if self._is_connected:
            del self._drive_peripheral
            del self._control_peripheral

            self._drive_peripheral = self._control_peripheral = None
            self._is_connected = False

    def _send_motor_control_command(self, left_speed: int, right_speed: int, msecs: int):
        """ Low-level control of the robot motors.

        :param int left_speed: speed of left motor, in ?? pr secs
        :param int right_speed: speed of right motor, in ?? pr secs
        :param int msecs: move duration, in milli-seconds
        """
        command = struct.pack('<Bhhh', self.CMD_DRIVE, left_speed, right_speed, msecs)
        self._write_to_drive(command)

    def _send_led_control_command(self, leds: int, red: int, green: int, blue: int):
        """ Low-level control of the robot LEDs.

        The targeted LEDs are specified as a bit pattern, built by combining the `LED_xxx` constants.

        :param int leds: targeted LEDs
        :param int red: red level
        :param int green: green level
        :param int blue: blue level
        """
        command = struct.pack('<BHBBB', self.CMD_LED, leds, red, green, blue)
        self._write_to_control(command)

    @needs_connection
    def drive(self, left_speed: int, right_speed: int, secs: float, wait=True):
        """ Drives the robot by settings the motors speed and the delay of the motion.

        Ozobot robots handle the duration of moves, and stop by themselves at the expiry
        of the delay. Speeds are signed 16 bit values.

        .. note::

            Unlike the low level command, the duration is expressed in seconds since this is
            a bit more human-friendly. The count can be a fractional value.

        :param int left_speed: speed of left motor, in ?? pr secs
        :param int right_speed: speed of right motor, in ?? pr secs
        :param float secs: move duration, in seconds
        :param bool wait: if True, the call is blocking until the delay expiry
        """
        self._send_motor_control_command(left_speed, right_speed, int(secs * 1000) & 0xffff)
        if wait:
            time.sleep(secs)

    @needs_connection
    def stop(self):
        """ Stops an ongoing move.
        """
        self._send_motor_control_command(0, 0, 0)

    @needs_connection
    def spin(self, speed: int, secs: float, wait=True):
        """ Spins the robot on itself.

        A positive spin speed makes the robot turn right (i.e. clockwise).

        :param int speed: spin speed, in ?? pr secs
        :param float secs: move duration, in seconds
        :param bool wait: if True, the call is blocking until the delay expiry
        """
        self._send_motor_control_command(speed, -speed, int(secs * 1000) & 0xffff)
        if wait:
            time.sleep(secs)

    @needs_connection
    def spin_right(self, speed: int, secs: float, wait=True):
        """ Shorthand method for right spinning.

        .. note::

            The sign of the passed speed is disregarded.
        """
        self.spin(abs(speed), secs, wait)

    @needs_connection
    def spin_left(self, speed: int, secs: float, wait=True):
        """ Shorthand method for left spinning.

        .. note::

            The sign of the passed speed is disregarded.
        """
        self.spin(-abs(speed), secs, wait)

    @needs_connection
    def set_leds(self, leds: Union[LedSelector, Iterable[LedSelector]], red: int, green: int, blue: int):
        """ Sets the color of the robot LEDs.

        Color level must be in the range [0, 255] inclusive.

        :param leds: an iterable of, or a single LedSelector
        :param int red: red level
        :param int green: green level
        :param int blue: blue level
        """
        try:
            # maybe it's a single selector
            led_pattern = leds.value
        except AttributeError:
            # No. It should be an iterable then
            led_pattern = reduce(lambda x, y: x | y, (led.value for led in leds))

        self._send_led_control_command(led_pattern, red, green, blue)

    @needs_connection
    def leds_off(self, leds: Union[LedSelector, Iterable[LedSelector]]):
        """ Turns some LEDs off.

        :param leds: an iterable of, or a single LedSelector
        """
        self.set_leds(leds, 0, 0, 0)

    @needs_connection
    def all_leds_off(self):
        """ Turns all LEDs off.
        """
        self._send_led_control_command(LED_ALL, 0, 0, 0)
