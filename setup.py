from setuptools import setup, find_packages


setup(
    name="ozoevo-lib",
    description="A package for controlling an OzoBot Evo via Bluetooth",
    version="0.1.0",
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    packages=find_packages(),
    python_requires='>3.5',
    author='Eric Pascual',
    install_requires=[
        'bluepy',
    ],
    tests_require=[
        'pytest',
        'pytest-runner'
    ],
    test_suite="tests",
)
